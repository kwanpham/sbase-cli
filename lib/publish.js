const execa = require('execa')
const chalk = require('chalk')
const semver = require('semver')
const inquirer = require('inquirer')
const path = require('path')
const fs = require('fs-extra')
const { stopSpinner, get, set, logWithSpinner, failSpinner } = require('../utils')
async function publish (moduleName) {
  const configPath = path.resolve(process.cwd(), 'module-configs.json')

  if (!fs.existsSync(configPath)) {
    console.log(chalk.red('Config path not found. Please specify a valid config path.'))
    process.exit(1)
  }

  const config = await fs.readJson(configPath)
  const modulePath = get(config, moduleName)

  const pkgPath = path.resolve(modulePath, 'package.json')
  const pkg = await fs.readJson(pkgPath)
  const curVersion = get(pkg, 'version')
  console.log(`Current version: ${curVersion}`)

  const bumps = ['patch', 'minor', 'major', 'prerelease']
  const versions = {}
  bumps.forEach(b => { versions[b] = semver.inc(curVersion, b) })
  const bumpChoices = bumps.map(b => ({ name: `${b} (${versions[b]})`, value: b }))

  const { bump, customVersion } = await inquirer.prompt([
    {
      name: 'bump',
      message: 'Select release type:',
      type: 'list',
      choices: [
        ...bumpChoices,
        { name: 'custom', value: 'custom' }
      ]
    },
    {
      name: 'customVersion',
      message: 'Input version:',
      type: 'input',
      when: answers => answers.bump === 'custom'
    }
  ])

  const version = customVersion || versions[bump]

  const { yes } = await inquirer.prompt([{
    name: 'yes',
    message: `Confirm releasing ${version}?`,
    type: 'confirm'
  }])

  if (yes) {
    try {
      await execa('git', ['add', '-A'], { stdio: 'inherit' })
      await execa('git', ['commit', '-m', 'pre release sync'], { stdio: 'inherit' })
      set(pkg, 'version', version)
      await fs.writeFile(pkgPath, JSON.stringify(pkg, null, 2), 'utf-8')

      console.log(`${pkgPath} file was saved`)
      logWithSpinner(`✨`, `Publishing module.`)

      await execa('cd', [`${modulePath}`])
      await execa('npm', ['publish'])
      console.log('Published.')

      return true
    } catch (e) {
      failSpinner(e.message)
      process.exit(1)
    }
  }
}

module.exports = (...args) => {
  return publish(...args).catch(err => {
    stopSpinner(false)
    console.error(err)
    process.exit(1)
  })
}
