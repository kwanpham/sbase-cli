const execa = require('execa')
const chalk = require('chalk')
const path = require('path')
const fs = require('fs-extra')
const { clearConsole, logWithSpinner, writeFileTree, stopSpinner, set } = require('../utils')
module.exports = class Creator {
  constructor(name, context) {
    this.name = name
    this.context = context
    this.run = this.run.bind(this)
  }

  run (command, args) {
    if (!args) { [command, ...args] = command.split(/\s+/) }
    return execa(command, args, { cwd: this.context })
  }

  async create () {
    const { name, context } = this

    await clearConsole()
    logWithSpinner(`✨`, `Creating module in ${chalk.yellow(context)}.`)

    const pkgFile = path.resolve(context, 'package.json')
    if (!fs.existsSync(pkgFile)) {
      // generate package.json
      const pkg = {
        name,
        version: '0.1.0',
        private: true,
        devDependencies: {}
      }

      // write package.json
      await writeFileTree(context, {
        'package.json': JSON.stringify(pkg, null, 2)
      })
    }

    //write config
    const file = path.resolve(process.cwd(), 'module-configs.json')
    if (fs.existsSync(file)) {
      const config = await fs.readJson(file)
      set(config, name, context)
      await fs.writeFile(file, JSON.stringify(config, null, 2), 'utf-8')
    } else {
      const config = {
        [name]: context
      }
      await fs.writeFile(file, JSON.stringify(config, null, 2), 'utf-8')
    }

    stopSpinner()
    console.log()
    console.log(`🎉  Successfully created module ${chalk.yellow(name)}.`)
  }
}
