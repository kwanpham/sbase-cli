const chalk = require('chalk')
const Creator = require('./Creator')
const fs = require('fs-extra')
const path = require('path')
const inquirer = require('inquirer')
const { clearConsole, stopSpinner } = require('../utils')
async function add (moduleName, options = {}) {
  console.log()
  console.log(`📦  Adding ${chalk.cyan(moduleName)}...`)
  console.log()

  const cwd = options.path || process.cwd()
  const inCurrent = moduleName === '.'
  const name = inCurrent ? path.relative('../', cwd) : moduleName
  const targetDir = path.resolve(cwd, moduleName || '.')

  if (fs.existsSync(targetDir)) {
    if (options.force) {
      await fs.remove(targetDir)
    } else {
      await clearConsole()
      if (inCurrent) {
        const {ok} = await inquirer.prompt([
          {
            name: 'ok',
            type: 'confirm',
            message: `Generate module in current directory?`
          }
        ])
        if (!ok) {
          return
        }
      } else {
        const { action } = await inquirer.prompt([
          {
            name: 'action',
            type: 'list',
            message: `Target directory ${chalk.cyan(targetDir)} already exists. Pick an action:`,
            choices: [
              { name: 'Overwrite', value: 'overwrite' },
              { name: 'Create', value: 'create' },
              { name: 'Cancel', value: false }
            ]
          }
        ])
        if (!action) {
          return
        } else if (action === 'overwrite') {
          console.log(`\nRemoving ${chalk.cyan(targetDir)}...`)
          await fs.remove(targetDir)
        }
      }
    }
  }

  const creator = new Creator(name, targetDir)
  await creator.create(options)
}

module.exports = (...args) => {
  return add(...args).catch(err => {
    stopSpinner(false)
    console.error(err)
    process.exit(1)
  })
}
