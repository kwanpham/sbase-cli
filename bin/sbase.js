#!/usr/bin/env node

const semver = require('semver');
const chalk = require('chalk');
const requiredVersion = require('../package.json').engines.node
const didYouMean = require('didyoumean')

// Setting edit distance to 60% of the input string's length
didYouMean.threshold = 0.6

function checkNodeVersion (wanted, id) {
  if (!semver.satisfies(process.version, wanted)) {
    console.log(chalk.red(
      'You are using Node ' + process.version + ', but this version of ' + id +
      ' requires Node ' + wanted + '.\nPlease upgrade your Node version.'
    ))
    process.exit(1)
  }
}

checkNodeVersion(requiredVersion, 'sbase-cli')

if (semver.satisfies(process.version, '9.x')) {
  console.log(chalk.red(
    `You are using Node ${process.version}.\n` +
    `Node.js 9.x has already reached end-of-life and will not be supported in future major releases.\n` +
    `It's strongly recommended to use an active LTS version instead.`
  ))
}

const minimist = require('minimist')
const program = require('commander')

program
  .version(require('../package').version)
  .usage('<command> [options]')


program
  .command('add <name> [moduleOptions]')
  .description('Create a module')
  .option('-p, --path <dir>', 'Use specified location to create new module')
  .option('-f, --force', 'Overwrite target directory if it exists')
  .allowUnknownOption()
  .action((name) => {
    require('../lib/add')(name, minimist(process.argv.slice(3)))
  })

program
  .command('publish <name> [moduleOptions]')
  .description('Publish a module to a remote npm registry')
  .allowUnknownOption()
  .action((name) => {
    require('../lib/publish')(name, minimist(process.argv.slice(3)))
  })

// output help information on unknown commands
program
  .arguments('<command>')
  .action((cmd) => {
    program.outputHelp()
    console.log(`  ` + chalk.red(`Unknown command ${chalk.yellow(cmd)}.`))
    console.log()
    suggestCommands(cmd)
  })

// add some useful info on help
program.on('--help', () => {
  console.log()
  console.log(`  Run ${chalk.cyan(`sbase <command> --help`)} for detailed usage of given command.`)
  console.log()
})

program.parse(process.argv)

if (!process.argv.slice(2).length) {
  program.outputHelp()
}

function suggestCommands (cmd) {
  const availableCommands = program.commands.map(cmd => {
    return cmd._name
  })

  const suggestion = didYouMean(cmd, availableCommands)
  if (suggestion) {
    console.log(`  ` + chalk.red(`Did you mean ${chalk.yellow(suggestion)}?`))
  }
}
